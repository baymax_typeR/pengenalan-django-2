from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse
from django.utils import timezone
# Create your models here.

class Peralatan(models.Model):
	KATEGORI_CHOICES = (
		('wd','Wood Working'),
		('el','Elektrik'),
		('mt','Metal Working'),
	)
	nama_alat = models.CharField('Nama Peralatan', max_length=50, null=False)
	kategori = models.CharField(max_length=2, choices= KATEGORI_CHOICES)
	keterangan = models.TextField()
	berat = models.FloatField('Berat (Kg)')
	harga = models.FloatField('Harga (Rp)')
	jumlah = models.IntegerField()
	tgl_input = models.DateTimeField('Tgl. Edit',default=timezone.now)
	user = models.ForeignKey(User, on_delete=models.DO_NOTHING)

	class Meta:
		ordering = ['-tgl_input']
					
	def __str__(self):
		return self.nama_alat

	def get_absolute_url(self):
		return reverse('home_page')

